using FreelanceAtTutors.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FreelanceAtTutors.Data
{
    public class FreelanceAtTutorsContext : IdentityDbContext<NewsUser>
    {
        public DbSet<Tutors> newList { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=FreelanceAtTutors.db");
        }
    }
}