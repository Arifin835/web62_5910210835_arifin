using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FreelanceAtTutors.Data;
using FreelanceAtTutors.Models;

namespace FreelanceAtTutors.Pages.TutorsAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FreelanceAtTutors.Data.FreelanceAtTutorsContext _context;

        public CreateModel(FreelanceAtTutors.Data.FreelanceAtTutorsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tutors Tutors { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newList.Add(Tutors);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}