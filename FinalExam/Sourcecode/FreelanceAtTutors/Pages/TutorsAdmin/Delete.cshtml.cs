using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FreelanceAtTutors.Data;
using FreelanceAtTutors.Models;

namespace FreelanceAtTutors.Pages.TutorsAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly FreelanceAtTutors.Data.FreelanceAtTutorsContext _context;

        public DeleteModel(FreelanceAtTutors.Data.FreelanceAtTutorsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tutors Tutors { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutors = await _context.newList.FirstOrDefaultAsync(m => m.TutorsID == id);

            if (Tutors == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tutors = await _context.newList.FindAsync(id);

            if (Tutors != null)
            {
                _context.newList.Remove(Tutors);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
