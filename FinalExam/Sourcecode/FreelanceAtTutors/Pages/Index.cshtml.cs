﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using FreelanceAtTutors.Data;
using FreelanceAtTutors.Models;

namespace FreelanceAtTutors.Pages
{
    public class IndexModel : PageModel
    {
        private readonly FreelanceAtTutors.Data.FreelanceAtTutorsContext _context;

        public IndexModel(FreelanceAtTutors.Data.FreelanceAtTutorsContext context)
        {
            _context = context;
        }

        public IList<Tutors> Tutors { get; set;}

        public async Task OnGetAsync()
        {
            Tutors = await _context.newList.ToListAsync();
        }
    }
}
